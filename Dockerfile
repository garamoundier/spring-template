# Use an official OpenJDK runtime as a parent image
FROM openjdk

# Set the working directory inside the container
WORKDIR /api

# Copy the JAR file (Spring Boot application) into the container
COPY target/demo-0.0.1-SNAPSHOT.jar /api/spring-api.jar

# Expose the port your Spring Boot application is running on (default is 8080)
EXPOSE 8080

# Command to run the Spring Boot application
CMD ["java", "-jar", "spring-api.jar"]
