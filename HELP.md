### Reference Documentation

Both docker-compose files work

Todo
- Remove docker build
- Include `overlay` instead of `bridge` 

Run with `sudo docker compose up`

Here are 3 Services
- pgadmin4 on ``
- postgresql on ``
- spring-api on ``

You can acces pgadmin through localhost:5051

1. Fill the login form with:
    - admin@gmail.com
    - admin

2. Create a Server
    - Give it a name like `MyServer` or whatever you please

3. Connection Tab:
    - host: `db`
    - port: `5432`
    - user: `postgres`
    - pass: `123456`

4. There you go! Try to perform `SELECT * FROM tbl_user`

Todo
- [] Hash slug idea
- [] Interlink lessons
- [] pending gitlab-ci 